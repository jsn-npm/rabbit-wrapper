import {IRabbitConnection} from '../types';

/**
 * @param {Required<IRabbitConnection>} connection_params
 * @return {string}
 */
export function buildConnectionURL(connection_params: Required<IRabbitConnection>): string{
    let url = `amqp://`;

    if(connection_params.user !== undefined && connection_params.user !== null && connection_params.user !== ''
        && connection_params.pass !== undefined && connection_params.pass !== null && connection_params.pass !== ''){
        url += `${connection_params.user}:${connection_params.pass}@`;
    }

    url += connection_params.host;

    if(connection_params.port !== undefined && connection_params.port !== null && connection_params.port !== 0)
        url += `:${connection_params.port}`;

    if(connection_params.vhost !== undefined && typeof connection_params.vhost !== null
        && connection_params.vhost !== '')
        url += '/' + connection_params.vhost.replace(/^\//, '');

    return url;
}
