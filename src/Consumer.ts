import {Channel, ConsumeMessage} from 'amqplib';

export abstract class Consumer{

    /**
     *
     */
    public constructor(){
        this.consume = this.consume.bind(this);
    }

    /**
     * @param {ConsumeMessage} message
     * @param {Channel} channel
     * @param {string} queue
     * @return {void | Promise<void>}
     */
    public abstract consume(message: ConsumeMessage, channel: Channel, queue: string): void|Promise<void>;

}
