import {Channel, ConsumeMessage} from 'amqplib';

export interface ConsumeMessageCallback{

    (message: ConsumeMessage, channel: Channel, queue_name: string): void|Promise<void>;

}
