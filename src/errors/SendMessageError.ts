import {RabbitError} from "./RabbitError";

export class SendMessageError extends RabbitError{}
