import {RabbitError} from "./RabbitError";

export class ConsumerCancellationError extends RabbitError{}
