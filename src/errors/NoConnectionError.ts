import {RabbitError} from "./RabbitError";

export class NoConnectionError extends RabbitError{}
