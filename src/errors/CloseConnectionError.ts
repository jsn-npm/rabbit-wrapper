import {RabbitError} from "./RabbitError";

export class CloseConnectionError extends RabbitError{}
