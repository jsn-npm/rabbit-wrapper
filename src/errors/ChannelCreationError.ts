import {RabbitError} from "./RabbitError";

export class ChannelCreationError extends RabbitError{}
