import {RabbitError} from "./RabbitError";

export class ConnectionError extends RabbitError{}
