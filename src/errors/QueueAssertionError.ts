import {RabbitError} from "./RabbitError";

export class QueueAssertionError extends RabbitError{}
