import {RabbitError} from "./RabbitError";

export class ConsumerError extends RabbitError{}
