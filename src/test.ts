import {Channel, ConsumeMessage} from 'amqplib';
import {Rabbit} from './Rabbit';

class Consumer{

    public readonly id: string;

    public constructor(id: string) {
        this.id = id;
        this.consume = this.consume.bind(this);
    }

    /**
     * @param {ConsumeMessage} message
     * @param {Channel} channel
     */
    public consume(message: ConsumeMessage, channel: Channel): void{
        console.log(`Consumer ( ${this.id} ): Received message: ${message.content.toString()}`);
        channel.ack(message);
    }

}

const consumer1 = new Consumer('john-snow'),
    consumer2 = new Consumer('sansa-stark');

const rabbit = new Rabbit({
    host: 'jsnow.in.ua',
    port: 5672,
    user: 'test-user',
    pass: '12345',
    vhost: 'testing',
    prefetch: 1,
    debug: true,
    handleSIGINT_SIGTERM: true
});

process.nextTick(async () => {
    console.log(rabbit.connectionURL);
    await rabbit.connect();
    await rabbit.consume('test-queue', consumer1.consume);
    console.log('Consumer ' + consumer1.id + ' set');
    await rabbit.consume('test-queue', consumer2.consume);
    console.log('Consumer ' + consumer2.id + ' set');
});
