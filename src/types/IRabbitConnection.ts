export interface IRabbitConnection {

    host?: string;
    port?: number;
    user: string;
    pass: string;
    vhost?: string;

}
