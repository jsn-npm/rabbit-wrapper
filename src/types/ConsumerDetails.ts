import {ConsumeMessageCallback} from '../callbacks';

export type ConsumerDetails = {

    tag: string;
    queue: string;
    callback: ConsumeMessageCallback;

};
