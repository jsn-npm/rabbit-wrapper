import {IRabbitConnection} from './IRabbitConnection';

export interface IRabbitConfig extends IRabbitConnection{

    prefetch?: number;
    debug?: boolean;
    handleSIGINT_SIGTERM?: boolean;

}
