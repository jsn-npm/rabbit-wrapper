type Type = string|number|boolean|object;

export type JSONSerializable = {

    [p: string]: Type|Type[]|JSONSerializable|JSONSerializable[];

};
