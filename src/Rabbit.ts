import {
    connect as connectAmqp,
    Connection as AmqpConnection,
    Channel as AmqpChannel, ConsumeMessage,
    Replies
} from 'amqplib';
import {buildConnectionURL} from "./utils";
import {
    QueueAssertionError,
    ChannelCreationError,
    ConnectionError,
    ConsumerCancellationError,
    NoConnectionError, ConsumerError, CloseConnectionError, SendMessageError
} from "./errors";
import {ConsumerDetails, IRabbitConfig, JSONSerializable, SendMessageHeaders} from './types';
import {ConsumeMessageCallback} from './callbacks';

export class Rabbit{

    protected params: Required<IRabbitConfig>;
    protected amqp_connection: AmqpConnection|null = null;
    protected amqp_channel: AmqpChannel|null = null;
    protected queues = new Map<string, Map<string, ConsumerDetails>>();
    protected asserted_channels: string[] = [];

    public get isConnected(): boolean{ return this._is_connected; }
    private _is_connected: boolean = false;

    /**
     * @returns {string}
     */
    public get connectionURL(): string {
        return buildConnectionURL(this.params);
    }

    /**
     * @param {IRabbitConfig} params
     */
    constructor(params: IRabbitConfig){
        this.params = Object.assign({}, <IRabbitConfig>{
            host: 'localhost',
            port: 5672,
            vhost: '',
            prefetch: 1,
            debug: false,
            handleSIGINT_SIGTERM: false
        }, params) as Required<IRabbitConfig>;

        this.onSignal = this.onSignal.bind(this);
    }

    /**
     * @param args
     * @protected
     */
    protected debug(...args: any[]): void{
        args.unshift(`[ ${this.constructor.name} ]`);
        this.params.debug && console.log.apply(console, args);
    }

    //region Connection

    /**
     * @returns {Promise<void>}
     * @throws {ConnectionError}
     * @throws {ChannelCreationError}
     * @throws {QueueAssertionError}
     */
    public async connect(): Promise<void>{
        if(this._is_connected)
            return;

        if(this.params.handleSIGINT_SIGTERM){
            process.on('SIGINT', this.onSignal);
            process.on('SIGTERM', this.onSignal);
        }

        try{
            await this.amqp_open_connection();
            await this.amqp_open_channel();
        }catch(err){
            throw err;
        }

        this._is_connected = true;
    }

    /**
     * @returns {Promise<void>}
     * @throws {ConnectionError}
     * @private
     */
    private async amqp_open_connection(): Promise<void>{
        let connection: AmqpConnection|null = null;
        try{
            connection = await connectAmqp(this.connectionURL, {});
        }catch(err){
            throw new ConnectionError();
        }

        this.amqp_connection = connection;
    }

    /**
     * @returns {Promise<void>}
     * @throws {ChannelCreationError}
     * @private
     */
    private async amqp_open_channel(): Promise<void>{
        let channel: AmqpChannel|null = null;
        try{
            channel = await (this.amqp_connection as AmqpConnection).createChannel();
            await channel.prefetch(this.params.prefetch);
        }catch(err){
            throw new ChannelCreationError();
        }

        this.amqp_channel = channel;
    }

    /**
     * @returns {Promise<void>}
     * @throws {QueueAssertionError}
     * @private
     */
    private async amqp_assert_queue(queue_name: string): Promise<void>{
        if(this.asserted_channels.indexOf(queue_name) !== -1)
            return;

        try{
            await (this.amqp_channel as AmqpChannel).assertQueue(queue_name);
        }catch(err){
            throw new QueueAssertionError();
        }

        this.asserted_channels.push(queue_name);
    }

    //endregion

    /**
     * @param {string} queue_name
     * @param {JSONSerializable} message
     * @param {SendMessageHeaders} [headers]
     * @returns {Promise<void>}
     * @throws {NoConnectionError}
     * @throws {QueueAssertionError}
     * @throws {SendMessageError}
     */
    public async send(queue_name: string, message: JSONSerializable, headers?: SendMessageHeaders): Promise<void>{
        if(!this._is_connected)
            throw new NoConnectionError();

        await this.amqp_assert_queue(queue_name);

        let encoded_message = JSON.stringify(message);
        let is_sent: boolean = false;

        try{
            is_sent = (this.amqp_channel as AmqpChannel).sendToQueue(queue_name, Buffer.from(encoded_message), {
                headers
            });
        }catch(err){
            throw new SendMessageError();
        }

        if(!is_sent)
            throw new SendMessageError();
    }

    /**
     * @param {string} queue_name
     * @param {ConsumeMessageCallback} callback
     * @throws {NoConnectionError}
     * @throws {QueueAssertionError}
     * @throws {ConsumerError}
     * @return {Promise<string>}
     */
    public async consume(queue_name: string, callback: ConsumeMessageCallback): Promise<string>{
        if(!this._is_connected)
            throw new NoConnectionError();

        await this.amqp_assert_queue(queue_name);

        if(!this.queues.has(queue_name))
            this.queues.set(queue_name, new Map<string, ConsumerDetails>());

        let reply: Replies.Consume|null = null;
        try{
            reply = await (this.amqp_channel as AmqpChannel).consume(
                queue_name,
                this.onMessage.bind(this, queue_name, callback), {
                    // Maybe pass parameters in next versions of wrapper
                });
        }catch(err){
            throw new ConsumerError();
        }

        (this.queues.get(queue_name) as Map<string, ConsumerDetails>).set(reply.consumerTag, {
            tag: reply.consumerTag,
            queue: queue_name,
            callback: callback
        });

        return reply.consumerTag;
    }

    /**
     * Cancel consumer by callback
     * @param {string} queue_name
     * @param {ConsumeMessageCallback} callback
     * @return {Promise<void>}
     */
    public async cancel(queue_name: string, callback: ConsumeMessageCallback): Promise<void>;
    /**
     * Cancel consumer by tag
     * @param {string} queue_name
     * @param {string} tag
     * @return {Promise<void>}
     */
    public async cancel(queue_name: string, tag: string): Promise<void>;
    public async cancel(queue_name: string, callbackOrTag: ConsumeMessageCallback|string): Promise<void>{
        if(!this._is_connected)
            return;

        if(!this.queues.has(queue_name))
            return;

        const queueConsumersList = this.queues.get(queue_name) as Map<string, ConsumerDetails>;
        let consumer: ConsumerDetails|null = null;
        if(typeof callbackOrTag === 'string'){
            if(queueConsumersList.has(callbackOrTag))
                return;

            consumer = queueConsumersList.get(callbackOrTag) as ConsumerDetails;
        }else if(typeof callbackOrTag === 'function'){
            for(const details of queueConsumersList.values()){
                if(details.callback === callbackOrTag){
                    consumer = details;
                    break;
                }
            }
        }

        if(consumer === null)
            return;

        queueConsumersList.delete(consumer.tag);
        try{
            await (this.amqp_channel as AmqpChannel).cancel(consumer.tag);
        }catch(err){
            throw new ConsumerCancellationError();
        }
    }

    /**
     * Cancel all consumers on all queues
     * @return {Promise<void>}
     */
    public async cancel_all(): Promise<void>;
    /**
     * Cancel all consumers on specific queue
     * @param {string} queue
     * @return {Promise<void>}
     */
    public async cancel_all(queue: string): Promise<void>;
    /**
     * @returns {Promise<void>}
     */
    public async cancel_all(queue?: string): Promise<void>{
        if(!this._is_connected)
            return;

        if(!queue){
            for(const queue of this.queues.keys())
                await this.cancel_all(queue);
        }else{
            if(!this.queues.has(queue))
                return;

            const queueConsumersList = this.queues.get(queue) as Map<string, ConsumerDetails>;
            for(const consumerDetails of queueConsumersList.values()){
                try{
                    await this.cancel(queue, consumerDetails.tag);
                }catch(e){}
            }
        }
    }

    /**
     * @returns {Promise<void>}
     * @throws {CloseConnectionError}
     */
    public async close(): Promise<void>{
        if(!this._is_connected)
            return;

        try{
            await this.cancel_all();
        }catch(err){}

        try{
            await (this.amqp_channel as AmqpChannel).close();
        }catch(err){}

        try{
            await (this.amqp_connection as AmqpConnection).close();
        }catch(err){
            throw new CloseConnectionError();
        }

        this.queues.clear();
        this.amqp_channel = null;
        this.amqp_connection = null;
        this._is_connected = false;
    }

    /**
     * @param {string} queue_name
     * @param {ConsumeMessageCallback} passToCallback
     * @param {ConsumeMessage | null} message
     * @protected
     */
    protected onMessage(queue_name: string, passToCallback: ConsumeMessageCallback, message: ConsumeMessage|null): void{
        if(!message)
            return;

        try{
            passToCallback(message, this.amqp_channel as AmqpChannel, queue_name);
        }catch(err){
        }
    }

    /**
     * @param {NodeJS.Signals | null} signal
     * @return {Promise<void>}
     * @protected
     */
    protected async onSignal(signal: NodeJS.Signals|null): Promise<void>{
        if(signal !== 'SIGINT' && signal !== 'SIGTERM')
            return;

        this.debug(`Received signal to terminate: ${signal}`);
        try {
            await this.close();
        }catch(e){
            this.debug(`Something went wrong during termination. Error message: ${e.message}`, e);
        }
    }

}
